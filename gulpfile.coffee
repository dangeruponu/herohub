gulp = require 'gulp'
gutil = require 'gulp-util'
uglify = require 'gulp-uglify'
coffee = require 'gulp-coffee'
watch = require 'gulp-watch'
concat = require 'gulp-concat'
flatten = require 'gulp-flatten'
minifycss = require 'gulp-minify-css'
size = require 'gulp-size'
less = require 'gulp-less'
jade = require 'gulp-jade'


path =
  scripts: 'client/scripts/**/*.coffee'
  styles: 'client/styles/**/*.less'
  bower: 'bower_components'
  templates: 'client/templates/**/*.jade'
  assets: 'client/assets/**/*'


gulp.task 'scripts', () ->
  gulp.src(path.scripts)
  .pipe(coffee({bare: true}).on 'error', (error) ->
      gutil.log error
      @emit 'end'
  )
  .pipe(concat 'app.min.js')
  .pipe(size())
  .pipe(gulp.dest 'public/js')

gulp.task 'styles', () ->
  gulp.src(path.styles)
  .pipe(less())
  .pipe(concat 'app.min.css')
  .pipe(minifycss())
  .pipe(size())
  .pipe(gulp.dest 'public/css')

gulp.task 'assets', () ->
  gulp.src(path.assets)
  .pipe(gulp.dest 'public/assets')

gulp.task 'templates', () ->
  gulp.src(path.templates)
  .pipe(jade())
  .pipe(size())
  .pipe(gulp.dest 'public/templates')
  .on 'error', (error) ->
    gutil.log error
    @emit 'end'

gulp.task 'watch', () ->
  gulp.watch path.scripts, ['scripts']
  gulp.watch path.styles, ['styles']
  gulp.watch path.templates, ['templates']
  gulp.watch path.assets, ['assets']

gulp.task 'jslib', () ->
  gulp.src(['bower_components/**/dist/*.min.js'])
  .pipe(concat 'lib.min.js')
  .pipe(size())
  .pipe(gulp.dest 'public/js')


gulp.task 'default', ['styles', 'templates', 'scripts', 'jslib', 'assets']

gulp.task 'dev', ['default', 'watch']
