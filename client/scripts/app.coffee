angular
	.module 'superhero', ['superhero.auth', 'superhero.validation', 'ui.router', 'ngMessages', 'ui.bootstrap', 'ui.mask']

	#
	#config
	#
	.config ['$httpProvider', ($httpProvider) ->
		$httpProvider.interceptors.push 'AuthInterceptor'
	]
	.config ['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
		$stateProvider
			.state 'nl', {
				abstract: true
				template: '<div ui-view/>'
				unSecured: true
				controller: ['$state', ($state) ->
					$state.go 'nl.index' if $state.current.name is 'nl'
				]
			}
			.state 'nl.index', {
				url: '/login'
				templateUrl: 'templates/appIndex.html'
				unSecured: true
			}
			.state 'nl.create', {
				url: '/create'
				templateUrl: 'templates/create.html'
				unSecured: true
			}
			.state 'app', {
				abstract: true
				templateUrl: 'templates/appContainer.html'
				controller: 'LoggedInAppController'
			}
			.state 'app.dash', {
				url: '/'
				templateUrl: 'templates/dash.html'
			}

		$urlRouterProvider.otherwise '/'
	]
	.run ['$rootScope', 'AuthService', '$state', 'RouterStateService', ($rootScope, AuthService, $state, RouterStateService) ->
		$rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams) ->
			if AuthService.hasValidToken() and toState.unSecured
				event.preventDefault()
				$state.go 'app.dash'
			else
				RouterStateService.setPrevious fromState.name, fromState.params
				if not AuthService.hasValidToken() and not toState.unSecured
					event.preventDefault()
					$state.go 'nl.index'

		AuthService.init()
	]

	#
	#Controllers
	#

	#
	#Directives
	#
	.directive 'bio', ['UserRepository', 'MadLibModal', '$timeout', (UserRepository, MadLibModal, $timeout) ->
		replace: true
		scope: true
		templateUrl: 'templates/bio.html'
		link: (scope) ->
			UserRepository.getBio()
			.then (response) ->
				scope.bio = response.data

			scope.generateBio = ->
				MadLibModal.open()
				.then (bio) ->
					scope.bio = bio

			scope.updateBio = ->
				UserRepository.updateBio scope.bio
				.then (response) ->
					scope.updated = true
					$timeout (-> scope.updated = false), 3000
	]

	.directive 'helpWanted', ['UserRepository', (UserRepository) ->
		replace: true
		scope: true
		templateUrl: 'templates/helpSearch.html'
		link: (scope) ->
			UserRepository.getHeroInfos()
			.then (response) ->
				scope.heroes = response.data
	]

	#
	#Filters
	#
	.filter 'phone', [->
		(phone) -> '(' + phone[0..2] + ') ' + phone[3..5] + '-' + phone[6..9]
	]

	#
	#Modals
	#
	.factory 'MadLibModal', ['$modal', 'MadLibRepository', ($modal, MadLibRepository) ->
		open: ->
			$modal.open {
				templateUrl: 'templates/madLibModal.html'
				controller: ['$scope', '$modalInstance', ($scope, $modalInstance) ->
					MadLibRepository.getRandomTemplateRequirements()
					.then (response) ->
						$scope.template =
							id: response.data.id
							requirements: {}
						$scope.requirements = response.data.requirements

					$scope.generate = (valid) ->
						if valid
							MadLibRepository.completeMadLib $scope.template
							.then (response) ->
								$modalInstance.close(response.data.madLib);

					$scope.cancel = $modalInstance.dismiss
				]
			}
			.result
	]

	#
	#Repositories
	#
	.factory 'MadLibRepository', ['$http', ($http) ->
		base = '/api/madLibs/'

		new class MadLibRepository
			getRandomTemplateRequirements: -> $http.get base + 'requirements'
			completeMadLib: (template) -> $http.post base, template
	]

	.factory 'UserRepository', ['$http', ($http) ->
		base = '/api/users/'
		new class UserRepository
			getBio: -> $http.get base + 'bio'
			updateBio: (bio) -> $http.post base + 'bio', {bio: bio}
			getHeroInfos: -> $http.get base + 'infos'
	]

	#
	#Services
	#
	.factory 'RouterStateService', () ->
		new class RouterStateService
			constructor: () ->
				@previousName = null
				@previousParams = null
			setPrevious: (stateName, stateParams) ->
				@previousName = stateName
				@previousParams = stateParams
			getPrevious: () -> {stateName: @previousName, stateParams: @previousParams}