angular
	.module 'superhero.auth', ['LocalStorageModule', 'ui.router']

	#
	#Repositories
	#
	.factory 'AuthRepository', ['$http', ($http) ->
		baseOpen = '/authentication/'
		new class AuthRepository
			login: (username, password) -> $http.post baseOpen + 'login', {username: username, password: password}
			create: (user) -> $http.post baseOpen, user
	]

	#
	#Services
	#
	.factory 'AuthService', ['localStorageService', '$timeout', '$state', '$rootScope', (localStorageService, $timeout, $state, $rootScope) ->
		new class AuthService
			logout: ->
				localStorageService.remove 'Token'
				localStorageService.remove 'ValidUntil'
				localStorageService.remove 'Principal'
				$rootScope.$broadcast 'Logout'
				$state.go 'nl.index'
			updateToken: (token, validUntil) ->
				localStorageService.set 'Token', token
				localStorageService.set 'ValidUntil', validUntil
				msToTimeout = validUntil - new Date().getTime()
				if @tokenTimeout
					$timeout.cancel @tokenTimeout
				@tokenTimeout = $timeout @logout, msToTimeout
			getToken: -> localStorageService.get 'Token'
			setToken: (token) -> localStorageService.set 'Token', token
			hasValidToken: ->
				validUntil = localStorageService.get 'ValidUntil'
				validUntil and validUntil > new Date().getTime() and @getToken()
			setPrincipal: (principal) ->
				@principal = principal
				localStorageService.set 'Principal', principal
				$rootScope.$broadcast 'principalUpdate'
			init: ->
				if @hasValidToken()
					@principal = localStorageService.get 'Principal'
					$state.go 'app.dash'

	]
	.factory 'AuthInterceptor', ['$injector', '$q', ($injector, $q) ->
		new class AuthInterceptor
			request: (config) ->
				@AuthService = $injector.get 'AuthService' if not @AuthService
				if @AuthService.hasValidToken()
					config.headers.Token = @AuthService.getToken()
				config
			response: (response) ->
				if response.headers 'Token'
					@AuthService.updateToken response.headers('Token'), parseInt(response.headers('ValidUntil'))
				response
			responseError: (response) ->
				if response.status is 417
					@AuthService.logout()
					console.log 'Invalid token. likely because the app was restarted and the user id\'s are no longer the same'
				$q.reject response
	]

	#
	#Controllers
	#
	.controller 'LoggedInAppController', ['$scope', 'AuthService', ($scope, AuthService) ->
		$scope.alias = AuthService.principal.alias
		$scope.logout = AuthService.logout
	]
	.controller 'CreateHeroController', ['$scope', 'AuthRepository', 'AuthService', '$state', ($scope, AuthRepository, AuthService, $state) ->
		$scope.hero = {}

		$scope.create = () ->
			AuthRepository.create $scope.hero
			.then (response) ->
				AuthService.setPrincipal response.data
				$state.go 'app.dash'
			, (error) ->
				if error.status is 409
					$scope.userAlreadyExists = true
	]

	#
	#Directives
	#
	.directive 'login', ['AuthService', 'AuthRepository', '$state', (AuthService, AuthRepository, $state) ->
		templateUrl: 'templates/login.html'
		replace: true
		scope: true
		link: (scope) ->
			scope.login = (valid) ->
				if not valid
					scope.highlightErrors = true
				else
					AuthRepository.login scope.username, scope.password
					.then (response) ->
						AuthService.setPrincipal response.data
						$state.go 'app.dash'
					, ->
						scope.invalidCreds = true
	]
