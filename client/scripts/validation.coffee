angular
	.module 'superhero.validation', []
	.directive 'passwordValidation', [->
		require: 'ngModel'
		link: (scope) ->
	]
	.directive 'compareTo', [->
		require: 'ngModel'
		scope:
			password: '=compareTo'
		link: (scope, elem, attrs, ngModel) ->
			ngModel.$validators.compareTo = (confVal) -> not scope.password or confVal is scope.password

			watchFn = -> ngModel.$validate()

			scope.$watch 'password', watchFn, true
	]
	.directive 'validationErrors', [->
		scope:
			formCtrl: '=validationErrors'
			overrides: '@?'
		replace: true
		templateUrl: 'templates/validationMessages.html'
		link:(scope) ->
			if scope.overrides
				scope.custom = scope.$eval(scope.overrides)
	]