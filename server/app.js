var express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    tokenAuthenticator = require('./security/tokenAuthenticator'),
	expressValidator = require('express-validator'),
	app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(path.join('client', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(function(req, res, next){
	req.isValid = function(){
		var errors = req.validationErrors();
		if (errors)
		{
			res.send(errors, 400);
			return false;
		}
		return true;
	};
	next();
});
app.use(cookieParser());
app.use(express.static('public'));


// ******
// Routes
// ******
var baseRoutes = require('./routes/index'),
	authRoutes = require('./routes/authRoutes'),
	userRoutes = require('./routes/userRoutes'),
	madLibRoutes = require('./routes/madLibRoutes');

app.use('/', baseRoutes);
app.use('/authentication', authRoutes.open);

app.all('/api/*', tokenAuthenticator);

app.use('/api/authentication', authRoutes.secured);
app.use('/api/madLibs', madLibRoutes);
app.use('/api/users', userRoutes);


// error handlers

app.all('*', function(req, res, next){
	res.status(404);
	res.send({ error: 'Not found' });
});


module.exports = app;
