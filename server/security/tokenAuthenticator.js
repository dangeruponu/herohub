var tokenService = require('./tokenService');

module.exports = function(req, res, next){
	if(req.method == 'OPTIONS') next();
	
	var token = req.headers.token || req.query.token;

	if (!token)
	{
		res.status(401);
		res.json({
			"status": 401,
			"message": "Invalid Token or Key"
		});
	}
	else
	{
		tokenService.decryptToken(token).then(
			function(tokenInfo){
				return tokenService.getPrincipal(tokenInfo.id).then(
					function(principal){
						req.principal = principal;
						tokenService.updateTokenHeaders(res, principal);
						next();
					}
				);
			}
		).then(
			null,
			function(err){
				res.status(417).send(err);
			}
		);
	}
};
