var jwt = require('jsonwebtoken'),
	secret = 'NooneKnowsMySecret',
	nodeCache = require('node-cache'),
	db = require('../db'),
	Principal = require('./principal'),
	stdTTLSeconds = 3600,
	tokenExpirationInMinutes = 60,
	authCache = new nodeCache({stdTTL: stdTTLSeconds}),
	q = require('q'),
	debug = require('debug')('tokenService:log'),
	_ = require('lodash');

exports.decryptToken = function(token){
	var deferred = q.defer();
	jwt.verify(token, secret, function(err, decoded){
		if (err)
			deferred.reject(err);
		else
			deferred.resolve(decoded);
	});
	return deferred.promise;
};

exports.getPrincipal = function(userId, forceRefresh) {
	var deferred = q.defer();

	try
	{
		if (forceRefresh)
		{
			authCache.del(userId);
		}

		authCache.get(userId, function(err, principal){
			if (!err)
			{
				if (!principal || forceRefresh)
				{
					debug('retrieving principal for userId=' + userId);

					var user = db.users.findById(userId);

					deferred.resolve(exports.cachePrincipal(user));
				}
				else
				{
					deferred.resolve(principal);
				}
			}
			else
			{
				deferred.reject(null);
			}
		});
	}
	catch(e)
	{
		deferred.reject(e);
	}

	return deferred.promise;
};

exports.cachePrincipal = function(user){
	var principal = new Principal(user);
	authCache.set(user.id, principal);
	return principal;
};

exports.updateTokenHeaders = function(res, principal){
	res.set('ValidUntil', new Date(new Date().getTime() + (stdTTLSeconds * 1000)).getTime());

	var token = jwt.sign(
	{
		id: principal.id,
		username: principal.username,
		name: principal.name
	}, secret, {expiresInMinutes: tokenExpirationInMinutes});

	res.set('Token', token);
};
