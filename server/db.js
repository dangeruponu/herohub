var _ = require('lodash'),
	uuid = require('node-uuid');

function Collection(startingData)
{
	this.allItems = startingData;
	_.forEach(this.allItems, function(item){
		item.id = uuid.v4()
	});

	this.findById = function(id){
		var obj = _.find(this.allItems, {id: id});
		if (obj)
			return _.cloneDeep(obj);
		return null;
	};

	this.query = function(query){
		var result = _.find(this.allItems, query);
		if (result)
			return _.cloneDeep(result);
		return null;
	};
}
Collection.prototype.save = function(item){
	if (!item.id)
	{
		item.id = uuid.v4();
		this.allItems.push(item);
	}
	else
	{
		var dbItem = _.find(this.allItems, {id: item.id});

		if (!dbItem)
			throw new Error('Item has id but doesn\'t exit in collection');

		_.extend(dbItem, item);
	}
};

function UserCollection(startingData){
	Collection.call(this, startingData);

	this.checkPassword = function(password, challenge){
		var decryptedPassword = password.substring(password.indexOf('_') + 1);
		return decryptedPassword === challenge;
	};

	this.save = function(item){
		if (!item.id)
			item.password = 'saltAndPepperPassword_' + item.password;

		Collection.prototype.save.call(this, item);
	};
}
UserCollection.prototype = Collection.prototype;


// allItems
var userCollection = new UserCollection([
	{secretIdentity: 'Steve Rogers', alias: 'Captain America', username: 'camerica', phone: '5551233214', password: 'password', bio: 'I am a war hero. Genetically engineered greatness. I fight against hydra.'},
	{secretIdentity: 'Tony Stark', alias: 'Iron Man', username: 'ironman', phone: '5559874565', password: 'password', bio: 'I am Iron Man. Need I say more?'},
	{secretIdentity: 'Natasha Romanova', alias: 'Black Widow', username: 'bwidow', phone: '5558675309', password: 'password', bio: 'I like to keep my business a secret. Make me upset and you\'ll regret it... dearly.'}
]);

var madLibs = new Collection([
	{template: 'My ~noun.Noun~ nearly ~pastTenseVerb.Past Tense Verb~ to death due to ~adjective1.Adjective~ ~pluralNoun.Plural Noun~ trying to take over my beloved ~place.Place~. The ~pluralNoun2.Plural Noun~ were able to thwart the attack, however I felt very ~feeling.Feeling~ afterwards, leading me to seek out a job as a superhero. I was ~feeling2.Feeling~ when I noticed my arch-nemisis ~archNemisis.Arch Nemisis~ ~verb.Verb~ his ~adjective.Adjective~ spaceship. From that day forward, I vowed never to feel ~feeling2.Feeling~ again, unsuccessfully. It was at this point I discovered my two real powers: ~superpower1.Super Power~ and ~superpower2.Super Power~. Hopefully they will be enough to stop ~archNemisis.Arch Nemisis~\'s ~adjective3.Adjective~ ~animal.Animal~!'},
	{template: 'A very long time ago, I ~pastV.Past Tense Verb~ while I saw the city. I thought, "~surp.Expression of Surprise (ex: Wow!)~ there are a lot of ~adj.Adjective~, ~adj2.Adjective~ ~pNoun.Plural Noun~ here. In a lapse of judgement, I decided to use my ~power.Super Power~ to destroy them. If it weren\'t for my love for ~love.A Love You Can\'t Live Without~, I would have destroyed every last one of them. After being one of the greatest villians ~verb1.-ing Verb~ the Earth, I now use my power to help the ~adj2.Adjective~ ~pNoun.Plural Noun~ of ~hometown.Home Town~.'},
	{template: 'When I was a young ~noun.Living Thing~, all the others would pick on me. They would call me ~rude.Rude Name~ and ~past3.Past Tense Verb~ while I suffered. After feeling ~feeling.Feeling~ for a while, then feeling ~feeling2.Feeling~ I had a moment of greatness. My ~power.Super Power~ could save the world. In my darkest moments, I realized that my powers were the key to ending ~problem.Problem in the World~. It was exactly what my favorite Miss America Pageant winners like ~name.Favorite Celebrity~ would do if they were able. There was only one group who ~past.Past Tense Verb~ in the way... ~name2.Celebrity You Hate~ and ~name3.Some Organization~. My ~nouns.Plural Noun~ are still no match for theirs, but through much ~v3.-ing Verb~ we will be victorious.'},
	{template: 'I\'m not technically a superhero. One time when I was ~f.A Feeling You Sometimes Get~, I invented the most elaborate ~invention.Invention~ ever seen. It was ~adjective.Adjective~ and unlike the others that preceeded it, it could ~v.Non -ing Verb~ on it\'s own. I know it\'s ~adj.Adjective~, but I love it. I was sad when I turned ~num.Number Greater Than Zero~ years old and still didn\'t have powers. Fortunately, I was smart and blazed my own trail to Hero Hub. Nothing can stop me except ~w.Weakness~, but I\'ll invent something to fix that too.'}
]);

module.exports = {
	users: userCollection,
	madLibs: madLibs
};