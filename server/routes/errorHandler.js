var debug = require('debug');

module.exports = function(name){
	var errorLog = debug(name + ":error");
	return function(res, err){
		var id = new Date().getTime(); 
		errorLog("errorId: " + id + " *****************************************************************************");
		errorLog(err);
		res.status(err.status ? err.status : 500).send({displayable: err.displayable, errorId: id, message: err.message});
	};
};