var express = require('express'),
	openRouter = express.Router(),
	securedRouter = express.Router(),
	tokenService = require('../security/tokenService'),
	errorHandler = require('./errorHandler')('auth'),
	userService = require('../api/userService');

openRouter.post('/login', function(req, res, next) {
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	if (req.isValid())
	{
		return userService.getUserByUsernameAndPassword(req.body.username, req.body.password).then(function(user){
			if (!user)
				return res.status(401).send('Username or password was incorrect');

			return loginWithUser(user, res).then(function(principal){
				res.send(principal);
			})
		}).then(null, function(err){
			errorHandler(res, err);
		});
	}
});

function loginWithUser(user, res){
	return tokenService.getPrincipal(user.id, true).then(function(principal){
		tokenService.updateTokenHeaders(res, principal);
		return principal;
	});
}

openRouter.post('/', function(req, res){
	req.checkBody('username').notEmpty();
	req.checkBody('password').notEmpty().matches(/^(?=.*\d).{6,20}$/);
	req.checkBody('secretIdentity').notEmpty();
	if (req.isValid())
	{
		return userService.createUser(req.body).then(function(user){
			return loginWithUser(user, res).then(function(principal){
				res.send(principal);
			})
		}).then(null, function(err){
			errorHandler(res, err);
		});
	}
});
//
//securedRouter.get('/refresh', function(req, res){
//	res.send(req.principal);
//});

module.exports = {
	open: openRouter,
	secured: securedRouter
};
