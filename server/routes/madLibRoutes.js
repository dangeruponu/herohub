var express = require('express'),
	router = express.Router(),
	errorHandler = require('./errorHandler')('madLibs'),
	madLibService = require('../api/madLibService');

router.get('/requirements', function(req, res) {
	madLibService.getRandomTemplateRequirements().then(function(requirements){
		res.send(requirements);
	});
});

router.post('/', function(req, res){
	req.checkBody('id').notEmpty();
	req.checkBody('requirements').notEmpty();
	if (req.isValid())
	{
		madLibService.buildMadLib(req.principal, req.body).then(function(madLibStr){
			res.send({madLib: madLibStr});
		}).then(null, function(err){
			errorHandler(err);
		});
	}
});

module.exports = router;