var express = require('express'),
	router = express.Router(),
	errorHandler = require('./errorHandler')('users'),
	userService = require('../api/userService');

router.get('/bio', function(req, res){
	userService.getBio(req.principal).then(function(bio){
		res.send(bio);
	}).then(null, function(err){
		errorHandler(res, err);
	});
});

router.post('/bio', function(req, res){
	userService.updateBio(req.principal, req.body.bio).then(function(){
		res.send();
	}).then(null, function(err){
		errorHandler(res, err);
	});
});

router.get('/infos', function(req, res){
	userService.getHeroInfos(req.principal).then(function(infos){
		res.send(infos);
	}).then(null, function(err){
		errorHandler(res, err);
	});
});

module.exports = router;