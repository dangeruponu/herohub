var Users = require('../db').users,
	q = require('q'),
	_ = require('lodash');

exports.getUserByUsernameAndPassword = function(username, password){
	var user = Users.query({username: username});
	if (user && Users.checkPassword(user.password, password))
		return q.when(user);
	return q.when(null);
};

exports.createUser = function(user){
	if (Users.query({username: user.username}))
		return q.reject({status: 409, message: 'Username already exists'});

	var dbUser = {username: user.username, password: user.password, secretIdentity: user.secretIdentity, alias: user.alias, phone: user.phone};
	Users.save(dbUser);
	return q.when(dbUser);
};

exports.getBio = function(principal){
	var user = Users.findById(principal.id);

	return q.when(user.bio);
};

exports.updateBio = function(principal, bio){
	var user = Users.findById(principal.id);
	user.bio = bio;
	Users.save(user);
	return q.when(user);
};

exports.getHeroInfos = function(principal){
	var infos = [];
	_.forEach(_.reject(Users.allItems, {id: principal.id}), function(user){
		infos.push({alias: user.alias, phone: user.phone, bio: user.bio});
	});
	return q.when(infos);
};