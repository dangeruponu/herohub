var MadLibs = require('../db').madLibs,
	_ = require('lodash'),
	q = require('q');

exports.getRandomTemplateRequirements = function(){
	var madLib = _.sample(MadLibs.allItems),
		regex = new RegExp(/~([^~]+)~/g),
		match = regex.exec(madLib.template),
		requirements = [],
		parts,
		reqMap = {};

	while (match != null)
	{
		parts = match[1].split('.');
		requirements.push({key: [parts[0]], name: parts[1]});
		match = regex.exec(madLib.template);
	}

	_.forEach(_.sortBy(requirements, 'name'), function(req){
		reqMap[req.key] = req.name;
	});

	return q.when({id: madLib.id, requirements: reqMap});
};

exports.buildMadLib = function(principal, templateInfo){
	var str = MadLibs.findById(templateInfo.id).template;

	_.forEach(templateInfo.requirements, function(value, key){
		var regex = new RegExp("~" + key + "\\\.[^~]+~", "g");
		str = str.replace(regex, value);
	});

	return q.when(str);
};